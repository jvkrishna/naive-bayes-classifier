package ml.naivebayes.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import ml.naivebayes.constants.NBConstants.Mode;

/**
 * Meta information for each document. Stores information about document Id,
 * category id, words and their frequencies.
 * 
 * @author krishnaj
 *
 */
public class Document implements Serializable {

	private static final long serialVersionUID = 5375115222360142683L;

	private int docId;
	private int categoryId;
	private Map<Integer, Integer> wordFrequency = new HashMap<>();
	private Mode mode;

	public int getDocId() {
		return docId;
	}

	public void setDocId(int docId) {
		this.docId = docId;
	}

	public Map<Integer, Integer> getWordFrequency() {
		return wordFrequency;
	}

	public void setWordFrequency(Map<Integer, Integer> wordFrequency) {
		this.wordFrequency = wordFrequency;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public Mode getMode() {
		return mode;
	}

	public void setMode(Mode mode) {
		this.mode = mode;
	}

}
