package ml.naivebayes.dto;

import java.io.Serializable;

/**
 * Stores category Id and category Name
 * @author krishnaj
 *
 */
public class Category implements Serializable {

	private static final long serialVersionUID = 1600833319895571246L;

	private int categoryId;
	private String categoryName;

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

}
