package ml.naivebayes.constants;

import java.util.Map;

/**
 * Stores Priors and Likelihoods information after the training.
 * 
 * @author krishnaj
 *
 */
public class LearningParameters {

	private final Map<Integer, Double> priorsMap;
	private final Map<Integer, Map<Integer, Double>> catWordConditionalProbs;

	public LearningParameters(Map<Integer, Double> priorsMap,
			Map<Integer, Map<Integer, Double>> catWordConditionalProbs) {
		super();
		this.priorsMap = priorsMap;
		this.catWordConditionalProbs = catWordConditionalProbs;
	}

	public Map<Integer, Double> getPriorsMap() {
		return priorsMap;
	}

	public Map<Integer, Map<Integer, Double>> getCatWordConditionalProbs() {
		return catWordConditionalProbs;
	}

}
