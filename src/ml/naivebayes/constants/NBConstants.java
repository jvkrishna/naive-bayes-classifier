package ml.naivebayes.constants;

import ml.naivebayes.process.learning.estimator.BayesianEstimator;
import ml.naivebayes.process.learning.estimator.Estimator;
import ml.naivebayes.process.learning.estimator.MLEstimator;

public class NBConstants {

	public enum Mode {
		TEST, TRAIN
	}

	public enum EstimatorType {
		MLE(new MLEstimator()), BAYESIAN(new BayesianEstimator());
		EstimatorType(Estimator estimator) {
			this.estimator = estimator;
		}

		private Estimator estimator;

		public Estimator getEstimator() {
			return estimator;
		}

	}

}
