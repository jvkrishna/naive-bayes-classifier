package ml.naivebayes.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ml.naivebayes.dto.Category;
import ml.naivebayes.dto.Document;

/**
 * Class to generate utility functions for Navive Bayes classifier.
 * 
 * @author krishnaj
 *
 */
public class NBUtils {
	static final Logger logger = LoggerFactory.getLogger(NBUtils.class);

	/**
	 * Reads vocabulary csv file and generates a Map with key as index and value
	 * as the word.
	 * 
	 * @param vocabFilePath
	 * @return
	 * @throws IOException
	 */
	public static Map<Integer, String> prepareVocabularyMap(String vocabFilePath) throws IOException {
		List<CSVRecord> records = parseCSVFile(vocabFilePath);
		Map<Integer, String> vocabulary = new HashMap<>();
		int count = 1;
		for (CSVRecord record : records) {
			vocabulary.put(count++, record.get(0));
		}

		return vocabulary;
	}

	/**
	 * Reads the data file CSV file and generates a list of {@link Document} It
	 * reads each line and generates a Document object by consolidating the
	 * document Id and Map of words and their frequencies.
	 * 
	 * @param dataFilePath
	 * @return
	 * @throws IOException
	 */
	public static List<Document> prepareDataSet(String dataFilePath) throws IOException {
		List<CSVRecord> dataRecords = parseCSVFile(dataFilePath);
		// Prepare Document Data
		Map<Integer, Document> documentsMap = new HashMap<>();
		for (CSVRecord record : dataRecords) {
			Integer docId = Integer.parseInt(record.get(0));
			Integer wordId = Integer.parseInt(record.get(1));
			Integer frequency = Integer.parseInt(record.get(2));
			Document document = documentsMap.get(docId);
			if (document == null) {
				document = new Document();
				document.setDocId(docId);
				documentsMap.put(docId, document);
			}
			document.getWordFrequency().put(wordId, frequency);
		}
		return new ArrayList<>(documentsMap.values());
	}

	/**
	 * Once the List of Documents are prepared, each document will be updated
	 * with their corresponding category Ids.
	 * 
	 * @param documents
	 * @param labelFilePath
	 * @throws IOException
	 */
	public static void updateDocumentsWithCategories(List<Document> documents, String labelFilePath)
			throws IOException {
		List<CSVRecord> labelRecords = parseCSVFile(labelFilePath);
		for (Document document : documents) {
			int categoryId = Integer.parseInt(labelRecords.get(document.getDocId() - 1).get(0));
			document.setCategoryId(categoryId);
		}
	}

	/**
	 * Reads data label CSV file and generates list of category Ids.
	 * 
	 * @param labelFilePath
	 * @return
	 * @throws IOException
	 */
	public static List<Integer> prepareCategoryIds(String labelFilePath) throws IOException {
		List<CSVRecord> labelRecords = parseCSVFile(labelFilePath);
		List<Integer> categoryIds = new ArrayList<>();
		for (CSVRecord record : labelRecords) {
			categoryIds.add(Integer.parseInt(record.get(0)));
		}
		return categoryIds;
	}

	/**
	 * Reads map csv file and generates a map with key as category Id and value
	 * as category Value
	 * 
	 * @param mapFilePath
	 * @return
	 * @throws IOException
	 */
	public static List<Category> prepareCategoryData(String mapFilePath) throws IOException {
		List<CSVRecord> records = parseCSVFile(mapFilePath);
		List<Category> categories = new ArrayList<>();
		Category category = null;
		for (CSVRecord record : records) {
			category = new Category();
			category.setCategoryId(Integer.parseInt(record.get(0)));
			category.setCategoryName(record.get(1));
			categories.add(category);
		}
		return categories;
	}

	/**
	 * Parses a given CSV file and generates list of records.
	 * 
	 * @param filePath
	 * @return
	 * @throws IOException
	 */
	public static List<CSVRecord> parseCSVFile(String filePath) throws IOException {
		File csvFile = new File(filePath);
		if (!csvFile.exists()) {
			logger.error("File: {} is not found", filePath);
			throw new IOException(filePath + "is not found");
		}
		CSVParser parser = CSVParser.parse(csvFile, Charset.defaultCharset(), CSVFormat.DEFAULT);
		return parser.getRecords();
	}

	/**
	 * Creates consolidated data by combining category Id and Word Id and their
	 * occurences.Similarly creating another map with category Id and total
	 * words associated with that category Id.
	 * 
	 * @param catDocMap
	 * @param catWordsMap
	 * @param totalWordsMap
	 */
	public static void createCatWordsMap(Map<Integer, List<Document>> catDocMap,
			Map<Integer, Map<Integer, Integer>> catWordsMap, Map<Integer, Integer> totalWordsMap) {
		for (Entry<Integer, List<Document>> catDoc : catDocMap.entrySet()) {
			int catId = catDoc.getKey();
			List<Document> docs = catDoc.getValue();
			int totalWords = 0;
			Map<Integer, Integer> catWordMap = new HashMap<>();
			for (Document doc : docs) {
				for (Entry<Integer, Integer> wordFreq : doc.getWordFrequency().entrySet()) {
					totalWords += wordFreq.getValue();
					// If the key already exists then update otherwise insert
					catWordMap.put(wordFreq.getKey(),
							catWordMap.getOrDefault(wordFreq.getKey(), 0) + wordFreq.getValue());

				}
			}
			catWordsMap.put(catId, catWordMap);
			totalWordsMap.put(catId, totalWords);
		}
	}

	/**
	 * Calculate the sum of all elements in the given array.
	 * 
	 * @param arr
	 * @return
	 */
	public static int arraySum(int[] arr) {
		int sum = 0;
		for (int i = 0; i < arr.length; i++) {
			sum += arr[i];
		}
		return sum;
	}

}
