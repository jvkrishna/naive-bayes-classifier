package ml.naivebayes.process.learning.estimator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ml.naivebayes.dto.Category;
import ml.naivebayes.dto.Document;
import ml.naivebayes.utils.NBUtils;

/**
 * Maximum Likelihood Estimation for calculating class conditional probabilities.
 * @author krishnaj
 *
 */
public class MLEstimator implements Estimator {
	private static final Logger logger = LoggerFactory.getLogger(MLEstimator.class);

	@Override
	public Map<Integer, Map<Integer, Double>> classConditionalEstimate(Map<Integer, List<Document>> catDocMap,
			List<Category> categories, Map<Integer, String> vocabulary) {
		// Prepare Data
		logger.debug("Preparing data for calculating likelihoods.");
		Map<Integer, Map<Integer, Integer>> catWordsMap = new HashMap<>();
		Map<Integer, Integer> totalWordsMap = new HashMap<>();
		NBUtils.createCatWordsMap(catDocMap, catWordsMap, totalWordsMap);
		// Calculate Likelihoods
		logger.info("Calculating Class Conditional Probabilities with Maximum Likelihood Estimator");
		Map<Integer, Map<Integer, Double>> catConditionalProbs = new HashMap<>();
		for (Category category : categories) {
			int catId = category.getCategoryId();
			Map<Integer, Integer> catWordMap = catWordsMap.get(catId);
			Map<Integer, Double> conditionalProbs = new HashMap<>();
			for (Integer wordId : vocabulary.keySet()) {
				double likelihood = (double) catWordMap.getOrDefault(wordId, 0) / (double) totalWordsMap.get(catId);
				logger.trace("Category : {} \t WordId: {} \t Likelihood:{} ",catId,wordId,likelihood);
				conditionalProbs.put(wordId, likelihood);
			}
			catConditionalProbs.put(catId, conditionalProbs);
		}
		return catConditionalProbs;
	}

}
