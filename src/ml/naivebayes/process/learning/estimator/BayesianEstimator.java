package ml.naivebayes.process.learning.estimator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ml.naivebayes.dto.Category;
import ml.naivebayes.dto.Document;
import ml.naivebayes.utils.NBUtils;

/**
 * Bayesian Estimation for calculating likelihoods.
 * 
 * @author krishnaj
 *
 */
public class BayesianEstimator implements Estimator {

	private static final Logger logger = LoggerFactory.getLogger(BayesianEstimator.class);

	@Override
	public Map<Integer, Map<Integer, Double>> classConditionalEstimate(Map<Integer, List<Document>> catDocMap,
			List<Category> categories, Map<Integer, String> vocabulary) {
		// Prepare Data
		logger.info("Preparing data for calculating Bayesian Esitmations.");
		Map<Integer, Map<Integer, Integer>> catWordsMap = new HashMap<>();
		Map<Integer, Integer> totalWordsMap = new HashMap<>();
		NBUtils.createCatWordsMap(catDocMap, catWordsMap, totalWordsMap);
		// Calculate Likelihoods
		logger.info("Calculating Class Conditional Probabilities with Bayesian Estimator with Laplace Esitmate.");
		int vocabSize = vocabulary.size();
		logger.info("Total Vocabulary: {}", vocabSize);
		Map<Integer, Map<Integer, Double>> catConditionalProbs = new HashMap<>();
		for (Category category : categories) {
			int catId = category.getCategoryId();
			Map<Integer, Integer> catWordMap = catWordsMap.get(catId);
			Map<Integer, Double> conditionalProbs = new HashMap<>();
			for (Integer wordId : vocabulary.keySet()) {
				double likelihood = (double) (catWordMap.getOrDefault(wordId, 0) + 1)
						/ (double) (totalWordsMap.get(catId) + vocabSize);
				logger.trace("Category : {} \t WordId: {} \t Likelihood:{} ", catId, wordId, likelihood);
				conditionalProbs.put(wordId, likelihood);
			}
			catConditionalProbs.put(catId, conditionalProbs);
		}
		return catConditionalProbs;

	}

}
