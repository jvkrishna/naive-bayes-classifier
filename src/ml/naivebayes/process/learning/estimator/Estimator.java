package ml.naivebayes.process.learning.estimator;

import java.util.List;
import java.util.Map;

import ml.naivebayes.dto.Category;
import ml.naivebayes.dto.Document;

/**
 * Estimator class for generating likelihood probabilities.
 * 
 * @author krishnaj
 *
 */
public interface Estimator {

	/**
	 * Estimates class conditional probabilities.These are like for each
	 * category, for each word, what is the probability of that word belongs to
	 * that categories,i.e. P(w|omega).
	 * 
	 * @param catDocMap
	 * @param categories
	 * @param vocabulary
	 * @return
	 */
	Map<Integer, Map<Integer, Double>> classConditionalEstimate(Map<Integer, List<Document>> catDocMap,
			List<Category> categories, Map<Integer, String> vocabulary);
}
