package ml.naivebayes.process.learning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ml.naivebayes.constants.LearningParameters;
import ml.naivebayes.constants.NBConstants.EstimatorType;
import ml.naivebayes.dto.Category;
import ml.naivebayes.dto.Document;

/**
 * Learning Model.This takes all the training documents and generates learning
 * parameters.
 * 
 * @author krishnaj
 *
 */
public class LearnModel {
	private static final Logger logger = LoggerFactory.getLogger(LearnModel.class);

	/**
	 * Creates learning parameters with the given estimator class.
	 * 
	 * @param documents
	 * @param categories
	 * @param vocabulary
	 * @param estimator
	 * @return
	 */
	public static Map<EstimatorType, LearningParameters> learn(List<Document> documents, List<Category> categories,
			Map<Integer, String> vocabulary) {
		logger.debug("Creating a map with categoriers and their list of documents.");
		// Each category has multiple documents. Grouping similar documents into
		// one category.
		Map<Integer, List<Document>> catDocMap = new HashMap<>();
		for (Document document : documents) {
			if (catDocMap.get(document.getCategoryId()) == null
					|| catDocMap.get(document.getCategoryId()).size() == 0) {
				catDocMap.put(document.getCategoryId(), new ArrayList<>());
			}
			catDocMap.get(document.getCategoryId()).add(document);
		}
		// Calculate Prior Probabilities.
		Map<Integer, Double> priorsMap = calculatePriors(catDocMap, categories, documents.size());

		// Calculate Likelihood values
		Map<Integer, Map<Integer, Double>> mleCatWordConditionalProbs = EstimatorType.MLE.getEstimator()
				.classConditionalEstimate(catDocMap, categories, vocabulary);

		Map<Integer, Map<Integer, Double>> bayesianCatWordConditionalProbs = EstimatorType.BAYESIAN.getEstimator()
				.classConditionalEstimate(catDocMap, categories, vocabulary);

		// Setup learning parameters
		Map<EstimatorType, LearningParameters> learningParamsMap = new HashMap<>();
		learningParamsMap.put(EstimatorType.MLE, new LearningParameters(priorsMap, mleCatWordConditionalProbs));
		learningParamsMap.put(EstimatorType.BAYESIAN,
				new LearningParameters(priorsMap, bayesianCatWordConditionalProbs));
		return learningParamsMap;
	}

	/**
	 * Calculates prior probabilities for each category.
	 * 
	 * @param catDocMap
	 * @param categories
	 * @param totalDocuments
	 * @return Map with key as category Id and value as the prior probability.
	 */
	private static Map<Integer, Double> calculatePriors(Map<Integer, List<Document>> catDocMap,
			List<Category> categories, int totalDocuments) {

		logger.info("Calculating Prior Probabilities");
		// Priors
		Map<Integer, Double> priorsMap = new HashMap<>();
		logger.debug("Total training documents :{}", totalDocuments);
		for (Category category : categories) {
			double prior = (double) catDocMap.get(category.getCategoryId()).size() / (double) totalDocuments;
			logger.info("P(omega={}) = {}", category.getCategoryId(), prior);
			priorsMap.put(category.getCategoryId(), prior);
		}
		return priorsMap;
	}
}
