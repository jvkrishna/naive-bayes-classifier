package ml.naivebayes.process.classification;

/**
 * Stores category Id and posterior probability information.
 * 
 * @author krishnaj
 *
 */
public class CategoryChance implements Comparable<CategoryChance> {
	private int categoryId;
	// Probability p(omega_j|d)
	private double chance;

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public double getChance() {
		return chance;
	}

	public void setChance(double chance) {
		this.chance = chance;
	}

	@Override
	public int compareTo(CategoryChance o) {
		return Double.compare(this.chance, o.getChance());
	}

}
