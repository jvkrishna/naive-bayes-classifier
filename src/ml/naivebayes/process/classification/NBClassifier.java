package ml.naivebayes.process.classification;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ml.naivebayes.constants.LearningParameters;
import ml.naivebayes.dto.Category;
import ml.naivebayes.dto.Document;

/**
 * Naive Bayes classifier.
 * 
 * @author krishnaj
 *
 */
public class NBClassifier {

	private static final Logger logger = LoggerFactory.getLogger(NBClassifier.class);

	/**
	 * Documents will be classified using learning parameters. Note that docs
	 * will be updated with classified categories.
	 * 
	 * @param learningParams
	 * @param docs
	 * @param categories
	 */
	public static void classify(LearningParameters learningParams, List<Document> docs, List<Category> categories) {
		Map<Integer, Double> priors = learningParams.getPriorsMap();
		Map<Integer, Map<Integer, Double>> catWordCondProbs = learningParams.getCatWordConditionalProbs();
		/*
		 * For each test document calculate the posterior for each category.
		 */
		for (Document doc : docs) {
			Map<Integer, Integer> wordMap = doc.getWordFrequency();
			List<CategoryChance> catChances = new ArrayList<>();
			CategoryChance catChance = null;
			for (Category category : categories) {
				catChance = new CategoryChance();
				int catId = category.getCategoryId();
				double probability = Math.log(priors.get(catId));
				Map<Integer, Double> wordCondProb = catWordCondProbs.get(catId);
				for (Entry<Integer, Integer> wordIdAndFreq : wordMap.entrySet()) {
					probability += wordIdAndFreq.getValue() * Math.log(wordCondProb.get(wordIdAndFreq.getKey()));
				}
				catChance.setCategoryId(catId);
				catChance.setChance(probability);
				catChances.add(catChance);
			}
			CategoryChance maxChanceCat = Collections.max(catChances);
			// Setting predicted categoryId.
			doc.setCategoryId(maxChanceCat.getCategoryId());
			logger.debug("docId : {}  with Probability: {} and  Classification: {}", doc.getDocId(),
					maxChanceCat.getChance(), doc.getCategoryId());
		}
	}

}
