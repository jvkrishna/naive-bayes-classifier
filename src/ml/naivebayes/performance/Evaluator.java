package ml.naivebayes.performance;

import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ml.naivebayes.utils.NBUtils;

public class Evaluator {

	private static final Logger logger = LoggerFactory.getLogger(Evaluator.class);

	/**
	 * Prints overall accuracy and class accuracies.
	 * @param actualIds
	 * @param expectedIds
	 */
	@SuppressWarnings("rawtypes")
	public static void evaluatePerformance(List<Integer> actualIds, List<Integer> expectedIds) {
		int total = actualIds.size();
		Collection correctIds = CollectionUtils.intersection(actualIds, expectedIds);
		logger.info("Overall Accuracy = {}", (double) correctIds.size() / total);
		logger.info("\t Class Accuracy:");
		int[][] confusionMatrix = getConfusionMatrix(actualIds, expectedIds);
		for (int i = 0; i < confusionMatrix.length; i++) {
			logger.info("Group {}: {}", i + 1, (double) confusionMatrix[i][i] / NBUtils.arraySum(confusionMatrix[i]));
		}
	}

	/**
	 * Creates confusion matrix
	 * @param actualIds
	 * @param expectedIds
	 * @return
	 */
	public static int[][] getConfusionMatrix(List<Integer> actualIds, List<Integer> expectedIds) {
		//Not extensible. Explicitly provided the size.
		int[][] confusionMatrix = new int[20][20];
		for (int i = 0; i < actualIds.size(); i++) {
			confusionMatrix[expectedIds.get(i) - 1][actualIds.get(i) - 1]++;
		}
		return confusionMatrix;
	}

	/**
	 * 
	 * @param actualIds
	 * @param expectedIds
	 */
	public static void printConfusionMatrix(List<Integer> actualIds, List<Integer> expectedIds) {
		logger.info("\n\n Confusion Matrix");
		int[][] confusionMatrix = getConfusionMatrix(actualIds, expectedIds);
		StringBuilder outputStr = new StringBuilder("\t");
		// Preparing Headers
		for (int i = 0; i < confusionMatrix.length; i++) {
			outputStr.append(i + 1).append("\t");
		}
		outputStr.append("\n\n");
		for (int i = 0; i < confusionMatrix.length; i++) {
			outputStr.append(i + 1).append("\t");
			for (int j = 0; j < confusionMatrix[i].length; j++) {
				outputStr.append(confusionMatrix[i][j]).append(" \t");
			}
			outputStr.append("\n");
		}
		logger.info(outputStr.toString());
	}
}
