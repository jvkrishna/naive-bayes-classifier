package ml.naivebayes.application;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ml.naivebayes.constants.LearningParameters;
import ml.naivebayes.constants.NBConstants;
import ml.naivebayes.constants.NBConstants.EstimatorType;
import ml.naivebayes.dto.Category;
import ml.naivebayes.dto.Document;
import ml.naivebayes.performance.Evaluator;
import ml.naivebayes.process.classification.NBClassifier;
import ml.naivebayes.process.learning.LearnModel;
import ml.naivebayes.utils.NBUtils;

/**
 * Naive Bayes Main Application.
 * 
 * @author krishnaj
 *
 */
public class NaiveBayes {
	static final Logger logger = LoggerFactory.getLogger(NaiveBayes.class);

	public static void main(String[] args) throws IOException {
		String vocabPath, mapPath, trainLabelPath, trainDataPath, testLabelPath, testDataPath;

		if (args.length != 6) {
			logger.error("Invalid Arguments: Must Provide 6 Arguments.");
			return;
		}
		vocabPath = args[0];
		mapPath = args[1];
		trainLabelPath = args[2];
		trainDataPath = args[3];
		testLabelPath = args[4];
		testDataPath = args[5];

		/**
		 * Data PreProcessing-- Converting files into predefined DS formats..
		 */
		Map<Integer, String> vocabulary = NBUtils.prepareVocabularyMap(vocabPath);
		List<Category> categories = NBUtils.prepareCategoryData(mapPath);
		List<Document> trainDocuments = NBUtils.prepareDataSet(trainDataPath);
		NBUtils.updateDocumentsWithCategories(trainDocuments, trainLabelPath);

		/**
		 * 2.1 --> Prepare learning parameters.
		 */
		Map<EstimatorType, LearningParameters> learningParamsMap = LearnModel.learn(trainDocuments, categories,
				vocabulary);

		/**
		 * 2.2 --> Performance on training data with Bayesian Estimators.
		 */
		// Classify Training documents with bayesian estimator.
		logger.info("\n\nPerformance on {} data with {} estimator", NBConstants.Mode.TRAIN, EstimatorType.BAYESIAN);
		NBClassifier.classify(learningParamsMap.get(EstimatorType.BAYESIAN), trainDocuments, categories);
		List<Integer> trainActualIds = new ArrayList<>();
		List<Integer> trainExpectedIds = NBUtils.prepareCategoryIds(trainLabelPath);
		for (Document doc : trainDocuments) {
			trainActualIds.add(doc.getCategoryId());
		}
		Evaluator.evaluatePerformance(trainActualIds, trainExpectedIds);
		Evaluator.printConfusionMatrix(trainActualIds, trainExpectedIds);

		// Classify Training documents with MLE estimator.
		logger.info("\n\nPerformance on {} data with {} estimator", NBConstants.Mode.TRAIN, EstimatorType.MLE);
		NBClassifier.classify(learningParamsMap.get(EstimatorType.MLE), trainDocuments, categories);
		trainActualIds = new ArrayList<>();
		for (Document doc : trainDocuments) {
			trainActualIds.add(doc.getCategoryId());
		}
		Evaluator.evaluatePerformance(trainActualIds, trainExpectedIds);
		Evaluator.printConfusionMatrix(trainActualIds, trainExpectedIds);

		/**
		 * 2.3 --> Performance on testing data with both MLE and Bayesian
		 * Esitmators.
		 */
		List<Document> testDocuments = NBUtils.prepareDataSet(testDataPath);
		List<Integer> testExpectedIds = NBUtils.prepareCategoryIds(testLabelPath);
		// Part-1 MLE
		logger.info("\n\nPerformance on {} data with {} estimator", NBConstants.Mode.TEST, EstimatorType.MLE);
		NBClassifier.classify(learningParamsMap.get(EstimatorType.MLE), testDocuments, categories);
		List<Integer> testActualIds = new ArrayList<>();

		for (Document doc : testDocuments) {
			testActualIds.add(doc.getCategoryId());
		}
		Evaluator.evaluatePerformance(testActualIds, testExpectedIds);
		Evaluator.printConfusionMatrix(testActualIds, testExpectedIds);

		// Part-2 BE
		logger.info("\n\nPerformance on {} data with {} estimator", NBConstants.Mode.TEST, EstimatorType.BAYESIAN);
		NBClassifier.classify(learningParamsMap.get(EstimatorType.BAYESIAN), testDocuments, categories);
		testActualIds = new ArrayList<>();

		for (Document doc : testDocuments) {
			testActualIds.add(doc.getCategoryId());
		}
		Evaluator.evaluatePerformance(testActualIds, testExpectedIds);
		Evaluator.printConfusionMatrix(testActualIds, testExpectedIds);

	}

}
